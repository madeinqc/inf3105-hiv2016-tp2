/*
   INF3105 - Structures de données et algorithmes
   TP2
*/

#if !defined(__STATSMOIS_H__)
#define __STATSMOIS_H__

#include <set>
#include "commande.h"
#include "inventaire.h"

using namespace std;

class StatsMois {
  public:
    StatsMois();

    /**
     * Ajoute une commande au palmarès des commandes et ajuste, au besoin, le meilleur mets.
     *
     * @param commande La commande à ajouter.
     */
    void ajouterCommande(Commande&);

    /**
     * Retourne le meilleur mets pour le mois courant.
     *
     * @return le meilleur mets du mois.
     */
    std::string getMeilleurMets();

    /**
     * Retourne le nombre de commandes du meilleur mets pour le mois courant.
     *
     * @return le nombre de commandes du meilleur mets.
     */
    int getMeilleurMetsQty();

    /**
     * Ajoute une recette dans le tableau de toutes les recettes du mois
     *
     * @param recette La recette à ajouter au tableau.
     */
    void ajouterRecette(std::string, ListeIngredients);

    /**
     * Affiche tous les ingrédients utilisés pour le mois courant et leur quantité.
     */
    void afficherIngredients();

  private:
    std::string meilleurMets;
    int quantiteMeilleurMets;
    map<std::string, int> commandes;
    map<std::string, ListeIngredients> recettes;

    /**
     * Remplace le meilleur mets courant pour celui qui a le plus de commandes.
     *
     * @param mets Le nouveau meilleur mets.
     */
    void setMeilleurMets(std::string);

    /**
     * Remplace la quantité de commandes du meilleur mets par celle du nouveau meilleur mets.
     *
     * @param quantité la nouvelle quantité du meilleur mets.
     */
    void setMeilleurMetQty(int);
};

#endif
