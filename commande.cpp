/*
  INF3105 - Structures de données et algorithmes
  TP2
*/

#include "commande.h"

Commande::Commande() {

}

Commande::Commande(Date pDate) {
    date = pDate;
}

void Commande::ajouterElement(std::string pElement, int pQuantite) {
   commande[pElement] += pQuantite;
}

map<std::string, int> Commande::getItems() {
    return commande;
}
Date Commande::getDate() {
    return date;
}

void Commande::setDate(Date pDate) {
    date = pDate;
}

std::istream& operator >> (std::istream& is, Commande& pCommande) {
    std::string chaine;
    Date date;
    is >> date;
    pCommande.setDate(date);
    is >> chaine;
    while (is && chaine != "---") {
        int quantite;
        is >> quantite;
        pCommande.ajouterElement(chaine, quantite);
        is >> chaine;
    }

    return is;
}