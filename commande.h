/*
   INF3105 - Structures de données et algorithmes
   TP2
*/

#include <map>
#include "date.h"
#include <set>

#if !defined(__COMMANDE_H__)
#define __COMMANDE_H__

using namespace std;

class Commande
{
  public:
    Commande();

    /**
     * Crée une nouvelle instance de Commande avec une date précisée.
     *
     * @param date La date à laquelle a eu lieu la commande.
     */
    Commande(Date);

    /**
     * Ajoute un item au tableau pour la commande.
     *
     * @param nom Le nom de l'item à ajouter.
     * @param quantite Le nombre de fois que l'item a été commandé.
     */
    void ajouterElement(std::string, int);

    /**
     * Retourne la liste de tous les items contenus dans la commande.
     *
     * @return Tous les items de la commande.
     */
    map<std::string, int> getItems();

    /**
     * Ajuste la date pour laquelle la commande a été effectuée.
     *
     * @param date La date de la commande.
     */
    void setDate(Date);

    /**
     * Retourne la date à laquelle la commande a été effectuée.
     *
     * @return date La date de la commande.
     */
    Date getDate();

  private:
    Date date;
    map<std::string, int> commande;
    friend std::istream& operator >> (std::istream&, Commande&);
};

#endif
