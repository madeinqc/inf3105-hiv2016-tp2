# Makefile pour TP2.
# Adaptez ce fichier au besoin.

# Options standard.
OPTIONS = -Wall

# L'option -g permet de générer les infos de débogage.
# Décommentez la ligne suivante si vous désirez utiliser un 
# débogueur.
# OPTIONS = -g -O0 -Wall

# Les options -O, -O1, -O2, -O3 permetent d'optimiser le code binaire produit.
# Décommentez la ligne suivante si avant la remise finale
#OPTIONS = -O2 -Wall


# Syntaxe : cible : dépendance1 dépendance2 ...
# Ensuite, la ou les ligne(s) débutant par une tabulation (\t) 
# donne les commandes pour construire une cible

debug: OPTIONS = -g -O0 -Wall
debug: tp2

tp2: tp2.cpp inventaire.o date.o commande.o statsMois.o
	g++ $(OPTIONS) -o tp2 tp2.cpp inventaire.o date.o commande.o statsMois.o

inventaire.o: inventaire.h inventaire.cpp
	g++ $(OPTIONS) -c -o inventaire.o inventaire.cpp
	
date.o: date.h date.cpp
	g++ $(OPTIONS) -c -o date.o date.cpp

commande.o: commande.h commande.cpp
	g++ $(OPTIONS) -c -o commande.o commande.cpp

statsMois.o: statsMois.h statsMois.cpp
	g++ $(OPTIONS) -c -o statsMois.o statsMois.cpp

clean :
	rm -f *.o
	rm -f tp2
	rm -f *~

