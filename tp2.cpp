/*
  INF3105 - Structures de données et algorithmes
  TP2
*/

#include <fstream>
#include <vector>
#include <map>
#include "inventaire.h"
#include "commande.h"
#include "statsMois.h"

using namespace std;

/**
 * Remplit le tableau qui contient tous les mois de l'année.
 *
 * @param le tableau qui contient les mois.
 */
void initStats(vector<StatsMois>& stats) {
  for (int i = 0; i < 12; i++) {
    stats.push_back(StatsMois());
  }
}

int main(int argc, const char** argv)
{
  istream* entree;
  ifstream* entreeFichier = NULL;
  if (argc == 2) {
    entree = entreeFichier = new ifstream(argv[1]);
    if (entree->fail()) {
      cerr << "Erreur d'ouverture du fichier '" << argv[1] << "'" << endl;
      return 1;
    }
  } else {
    cerr << "Nom du fichier manquant." << endl;
    return 1;
  }


  //  À Compléter:
  // - Déclaration du dictionnaire de recettes.
  map<std::string, ListeIngredients> recettes;
  //- Déclaration des structures de données pour les palamarès
  // et les statistiques mensuelles des quantités d'ingrédients demandés
  Inventaire inventaire;
  Date date(1, 1, 16);
  vector<StatsMois> statistiques;
  initStats(statistiques);

  StatsMois sm = StatsMois();

  while (*entree) {
    string typeEvenement;
    *entree >> typeEvenement;

    if (!(*entree)) {
      break;
    }

    if (typeEvenement == "recette")
    {
      string nom;
      *entree >> nom;
      ListeIngredients *ingredients = new ListeIngredients();
      *entree >> *ingredients;
      recettes[nom] = *ingredients;

      // Lorsqu'il y a une nouvelle recette, on l'ajoute pour le mois courant et les mois à venir.
      for (int i = date.getMois()-1; i < 12; i++) {
        statistiques[i].ajouterRecette(nom, *ingredients);
      }
    }
    else if (typeEvenement == "livraison") {
      Inventaire inventaireRecu;
      *entree >> inventaireRecu;
      inventaire += inventaireRecu;
    } else if (typeEvenement == "commande") {
      Commande commande;
      *entree >> commande;

      // On ajoute la commande au mois courant pour les statistiques.
      statistiques[commande.getDate().getMois()-1].ajouterCommande(commande);
      if (commande.getDate() < date) {
        cout << "Attention : ce programme supporte uniquement un ordre chronologique!" << endl;
      }

      // Vérifier si l'inventaire est suffisant
      ListeIngredients toutesRecettes;
      map<string, int> items = commande.getItems();
      for (map<string, int>::const_iterator it = items.begin();
           it != items.end();
           ++it) {
        ListeIngredients recetteCourante = recettes[it->first];
        recetteCourante *= it->second;
        toutesRecettes += recetteCourante;
      }

      bool commandeAcceptee = inventaire.peutSatisfaire(toutesRecettes, commande.getDate());

      cout << commande.getDate() << " - ";

      if (commandeAcceptee) {
//        cout << "Before: " << endl;
//        for (map<string, Inventaire::InventaireExpirant>::const_iterator it = inventaire.ingredients.begin(); it != inventaire.ingredients.end(); ++it) {
//          int sum = 0;
//          Inventaire::InventaireExpirant inventaireExpirant = inventaire.ingredients[it->first];
//          for (map<Date, int>::iterator itCount = inventaireExpirant.ingrediantsQuantite.begin(); itCount != inventaireExpirant.ingrediantsQuantite.end(); ++itCount) {
//            cout << "\t\t" << it->first << ": " << itCount->first << " - " << itCount->second << endl;
//            sum += itCount->second;
//          }
//          cout << "\t" << it->first << ": " << sum << endl;
//        }
//        cout << "Minus: " << endl;
//        for (map<string, int>::const_iterator it = toutesRecettes.quantites.begin(); it != toutesRecettes.quantites.end(); ++it) {
//          cout << "\t" << it->first << ": " << it->second << endl;
//        }
        inventaire -= toutesRecettes;
//        cout << "After: " << endl;
//        for (map<string, Inventaire::InventaireExpirant>::const_iterator it = inventaire.ingredients.begin(); it != inventaire.ingredients.end(); ++it) {
//          int sum = 0;
//          Inventaire::InventaireExpirant inventaireExpirant = inventaire.ingredients[it->first];
//          for (map<Date, int>::iterator itCount = inventaireExpirant.ingrediantsQuantite.begin(); itCount != inventaireExpirant.ingrediantsQuantite.end(); ++itCount) {
//            cout << "\t\t" << it->first << ": " << itCount->first << " - " << itCount->second << endl;
//            sum += itCount->second;
//          }
//          cout << "\t" << it->first << ": " << sum << endl;
//        }
        cout << "OK" << endl;
      } else {
        cout << "Echec" << endl;
      }
    } else {
      cout << "Type d'évènement '" << typeEvenement << "' inconnu!" << endl;
      return 1;
    }
  }

  cout << "---" << endl;

  // Dans l'exemple de résultat obtenu, il n'y a pas d'accent pour les mois.
  std::string mois[] = {"Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin",
                 "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"};

  // On affiche le mets préféré pour chacun des mois.
  for (unsigned int i = 0; i < statistiques.size(); i++) {
    cout << mois[i] << ":" << statistiques[i].getMeilleurMets() << endl;
  }
  cout << "---" << endl;

  // On affiche tous les ingrédients utilisés pour chacun des mois.
  for (unsigned int i = 0; i < statistiques.size(); i++) {
    cout << mois[i] << ":";
    statistiques[i].afficherIngredients();
  }

  delete entreeFichier; // delete est sécuritaire même si entreeFichier==NULL
  return 0;
}
