/*
  INF3105 - Structures de données et algorithmes
  TP2
*/

#include "inventaire.h"

ListeIngredients& ListeIngredients::operator+=(const ListeIngredients& autre)
{
  for (std::map<std::string, int>::const_iterator it = autre.quantites.begin();
       it != autre.quantites.end();
       ++it) {
    this->quantites[it->first] += it->second;
  }
  return *this;
}

ListeIngredients& ListeIngredients::operator*=(int facteur)
{
  for (std::map<std::string, int>::const_iterator it = quantites.begin();
       it != quantites.end();
       ++ it) {
    quantites[it->first] *= facteur;
  }
  return *this;
}

std::istream& operator >> (std::istream& is, ListeIngredients& liste)
{
  std::string chaine;
  is >> chaine;
  while (is && chaine != "---") {
    int quantite;
    is >> quantite;

    liste.quantites[chaine] = quantite;

    is >> chaine;
  }
  return is;
}

Inventaire& Inventaire::operator+=(const Inventaire& autre)
{
  for (std::map<std::string, InventaireExpirant>::const_iterator it = autre.ingredients.begin();
       it != autre.ingredients.end();
       ++it) {
    InventaireExpirant inventaireExpirant = autre.ingredients.at(it->first);
    for (std::map<Date, int>::const_iterator itExpirant = inventaireExpirant.ingrediantsQuantite.begin();
         itExpirant != inventaireExpirant.ingrediantsQuantite.end();
         ++itExpirant) {
        this->ingredients[it->first].ingrediantsQuantite[itExpirant->first] += itExpirant->second;
    }
  }
  return *this;
}

Inventaire& Inventaire::operator-=(const ListeIngredients& liste)
{
  for (std::map<std::string, int>::const_iterator it = liste.quantites.begin();
       it != liste.quantites.end();
       ++it) {
    int requis = it->second;
//    std::cout << it->first << " requis: " << it->second << std::endl;
    InventaireExpirant inventaireExpirant = ingredients[it->first];
    for (std::map<Date, int>::const_iterator itExpirant = inventaireExpirant.ingrediantsQuantite.begin();
         requis != 0 && itExpirant != inventaireExpirant.ingrediantsQuantite.end();
         ++itExpirant) {
      int qt = itExpirant->second;
//      std::cout << it->first << " expirant: " << itExpirant->first << " - " << qt << std::endl;
      if (qt < requis) {
        requis -= qt;
        ingredients[it->first].ingrediantsQuantite[itExpirant->first] = 0;
//        std::cout << "Not enough: " << it->first << " requis: " << qt << std::endl;
      } else {
//        std::cout << "Right before: " << ingredients[it->first].ingrediantsQuantite[itExpirant->first] << std::endl;
        ingredients[it->first].ingrediantsQuantite[itExpirant->first] -= requis;
        requis = 0;
//        std::cout << "More than required: " << it->first << " Restant: " << ingredients[it->first].ingrediantsQuantite[itExpirant->first] << std::endl;
      }
    }
  }
  return *this;
}

bool Inventaire::peutSatisfaire(ListeIngredients liste, Date dateCommande) {
  for (std::map<std::string, int>::const_iterator it = liste.quantites.begin();
       it != liste.quantites.end();
       ++it) {
    int requis = it->second;
    int cumule = 0;
    InventaireExpirant inventaireExpirant = ingredients[it->first];
    for (std::map<Date, int>::const_iterator itExpirant = inventaireExpirant.ingrediantsQuantite.begin();
         requis != cumule && itExpirant->first > dateCommande && itExpirant != inventaireExpirant.ingrediantsQuantite.end();
         ++itExpirant) {
      int qt = itExpirant->second;
      if (qt < requis - cumule) {
        cumule += qt;
      } else {
        cumule = requis;
      }
    }
    if (cumule != requis) {
//      std::cout << "Could not satisfy " << it->first << ". Only got " << cumule << "/" << requis << std::endl;
      return false;
    }
  }
  return true;
}

std::istream& operator >> (std::istream& is, Inventaire& inventaire) {
  Date reception;
  std::string chaine;
  is >> reception;
  is >> chaine;
  while(is && chaine!="---") {
    int quantite;
    Date expiration;
    is >> quantite;
    is >> expiration;

    inventaire.ingredients[chaine].ingrediantsQuantite[expiration] += quantite;

    is >> chaine;
  }
  return is;
}
