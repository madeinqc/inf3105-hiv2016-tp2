/*
  INF3105 - Structures de donn�es et algorithmes
  TP2
*/

#include <assert.h>
#include "date.h"

Date::Date() {
  jour = 0;
  mois = 0;
  annee = 0;
}

Date::Date(int pJour, int pMois, int pAnnee) {
  jour = pJour;
  mois = pMois;
  annee = pAnnee;
}

Date::Date(const Date& autre) {
  jour = autre.jour;
  mois = autre.mois;
  annee = autre.annee;
}

Date::~Date() {
  // Nothing to do
}

int Date::getMois() {
  return mois;
}

bool operator<(Date const &lhs, Date const &rhs) {
  int result = lhs.annee - rhs.annee;
  if (result != 0)
    return result < 0;
  result = lhs.mois - rhs.mois;
  if (result != 0)
    return result < 0;

  return (lhs.jour - rhs.jour) < 0;
}

bool operator>(Date const &lhs, Date const &rhs) {
  return rhs < lhs;
}

bool operator>=(Date const &lhs, Date const &rhs) {
  return !(lhs < rhs);
}

bool operator<=(Date const &lhs, Date const &rhs) {
  return !(lhs > rhs);
}

bool operator==(Date const &lhs, Date const &rhs) {
  return lhs.annee == rhs.annee &&
    lhs.mois == rhs.mois &&
    lhs.jour == rhs.jour;
}

bool operator!=(Date const &lhs, Date const &rhs) {
  return !(lhs == rhs);
}

std::ostream& operator << (std::ostream& os, const Date& date) {
  os << date.jour
    << ':'
    << date.mois
    << ':'
    << date.annee;
  return os;
}

std::istream& operator >> (std::istream& is, Date& date) {
  char sep1, sep2;
  is >> date.jour;
  is >> sep1;
  assert(sep1 == ':');
  is >> date.mois;
  is >> sep2;
  assert(sep2 == ':');
  is >> date.annee;
  return is;
}
