/*
   INF3105 - Structures de données et algorithmes
   TP2
*/

#if !defined(__DATE_H__)
#define __DATE_H__

#include <iostream>

class Date{
  public:
    Date();

    /**
     * Crée une nouvelle instance de Date avec une date précisée.
     *
     * @param jour Le jour de la date.
     * @param mois Le mois de la date.
     * @param annee L'année de la date.
     */
    Date(int jour, int mois, int annee);

    /**
     * Crée une nouvelle instance de Date à partir d'une autre Date.
     *
     * @param date La date.
     */
    Date(const Date&);
    ~Date();

    /**
     * Retourne le mois sous forme de numéro, où janvier = 1, février = 2, etc.
     *
     * @return mois Le mois sous forme de numéro.
     */
    int getMois();

  private:
    int annee;
    int mois;
    int jour;

    friend std::ostream& operator << (std::ostream&, const Date&);
    friend std::istream& operator >> (std::istream&, Date&);

    friend bool operator<=(Date const &lhs, Date const &rhs);
    friend bool operator<(Date const &lhs, Date const &rhs);
    friend bool operator>(Date const &lhs, Date const &rhs);
    friend bool operator>=(Date const &lhs, Date const &rhs);
    friend bool operator==(Date const &lhs, Date const &rhs);
    friend bool operator!=(Date const &lhs, Date const &rhs);
};

#endif

