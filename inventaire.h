/*
   INF3105 - Structures de données et algorithmes
   TP2
*/

#if !defined(__INVENTAIRE_H__)
#define __INVENTAIRE_H__

#include <iostream>
#include <map>
#include "date.h"

class ListeIngredients
{
public:
  ListeIngredients& operator+=(const ListeIngredients& liste);
  ListeIngredients& operator*=(int quantite);

  std::map<std::string, int> quantites;
private:
  friend std::istream& operator >> (std::istream&, ListeIngredients&);
  friend class Inventaire;
};

class Inventaire
{
public:
  Inventaire& operator+=(const Inventaire&);
  Inventaire& operator-=(const ListeIngredients&);

  bool peutSatisfaire(ListeIngredients ingredients, Date dateCommande);

  class InventaireExpirant
  {
  public:
    std::map<Date, int> ingrediantsQuantite;
  };

  std::map<std::string, InventaireExpirant> ingredients;
private:

  friend std::istream& operator >> (std::istream&, Inventaire&);
};

#endif
