/*
   INF3105 - Structures de données et algorithmes
   TP2
*/

#include <set>
#include "statsMois.h"

StatsMois::StatsMois() {
  quantiteMeilleurMets = 0;
  meilleurMets = "-";
}

void StatsMois::ajouterCommande(Commande &pCommande) {
  map<std::string, int> items = pCommande.getItems();

  // On prend tous les items de la commande et on actualise le palmarès des mets.
  for (map<std::string, int>::iterator it = items.begin(); it != items.end(); ++it) {
    commandes[it->first] += it->second;
    if (commandes[it->first] > getMeilleurMetsQty()) {
      setMeilleurMets(it->first);
      setMeilleurMetQty(commandes[it->first]);
    }
  }
}

std::string StatsMois::getMeilleurMets() {
  return meilleurMets;
}

int StatsMois::getMeilleurMetsQty() {
  return quantiteMeilleurMets;
}

void StatsMois::setMeilleurMets(std::string pMets) {
  meilleurMets = pMets;
}

void StatsMois::setMeilleurMetQty(int pQty) {
  quantiteMeilleurMets = pQty;
}

void StatsMois::ajouterRecette(string nom, ListeIngredients ingredients) {
  recettes[nom] = ingredients;
}

void StatsMois::afficherIngredients() {
  if (getMeilleurMetsQty() == 0) {
    cout << "-" << endl;
  } else {
    cout << "\n";

    // On prend toutes les commandes, et pour chaque recette, on multiplie le nombre d'ingrédients utilisés par
    // le nombre de fois que la recette a été commandée.
    ListeIngredients ingredients;
    for (map<std::string, int>::iterator itCommandes = commandes.begin(); itCommandes != commandes.end(); ++itCommandes) {
      ListeIngredients listeIngredients = recettes[itCommandes->first];
      listeIngredients *= itCommandes->second;
      ingredients += listeIngredients;
    }
    for (map<std::string, int>::iterator it = ingredients.quantites.begin(); it != ingredients.quantites.end(); ++it) {
      cout << it->first << " " << it->second << endl;
    }
  }
}
